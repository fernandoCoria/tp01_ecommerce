-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 08-07-2020 a las 02:42:21
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tp01_a`
--
CREATE DATABASE IF NOT EXISTS `tp01_a` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `tp01_a`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

DROP TABLE IF EXISTS `productos`;
CREATE TABLE `productos` (
  `nombre` varchar(500) NOT NULL,
  `precio` varchar(500) NOT NULL,
  `foto` varchar(500) NOT NULL,
  `cantidad` varchar(500) NOT NULL,
  `descripcion` varchar(500) NOT NULL,
  `id` int(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`nombre`, `precio`, `foto`, `cantidad`, `descripcion`, `id`) VALUES
(' Smart TV Noblex DJ43X5100 LED Full HD 43\"', '38500', 'https://http2.mlstatic.com/D_NQ_NP_944297-MLA40720842618_022020-O.webp', '10', 'Tipo de resolución Full HD , pantalla: LED 43\" resolucion:1920px x 1080px. Dimensiones: 107 x 13 x 67 cm.', 1),
('Smart TV Sony Bravia KD-49X725F LED 4K 49\"', '75000', 'https://http2.mlstatic.com/D_NQ_NP_612407-MLA40927047519_022020-O.webp', '14', 'Tipo de resolucion 4K, Pantalla; 49\", Resolución máxima; 3840 x 2160 px, Dimensiones: 110 x 8 x 65 cm.', 2),
('Smart TV Sony Bravia XBR-65X805G LED 4K 65\"', '140000', 'https://http2.mlstatic.com/D_NQ_NP_783822-MLA40915968569_022020-O.webp', '15', 'Tipo de resolución 4k, Pantalla: LED 65\", Resolución máxima: 3840 x 2160 px, Dimensiones: 146 x 8 x 84 cm.', 3),
('Smart TV Samsung Series 6 QN65Q60RAGCZB QLED 4K 65\"', '190000', 'https://http2.mlstatic.com/D_NQ_NP_675578-MLA41443064730_042020-O.webp', '11', 'Tipo de resolución QLED, Pantalla:65\", Resolución máxima: 3840 x 2160 px, Dimensiones: 145 x 6 x 84 cm.', 4),
('Smart TV Samsung Series 7 UN75RU7100GXZD LED 4K 75\"', '290000', 'https://http2.mlstatic.com/D_NQ_NP_745957-MLA40768066247_022020-O.webp', ' 8', 'Tipo de resolución 4k, Pantalla: LEd 75\", Modo de sonido: Dolby digital plus, Resolucion máxima: 3840 px x 2160 px, Dimensiones: 168 x 6 x 96', 5),
('Smart TV Sony XBR-65A8G OLED 4K 65\"', '370000', ' https://http2.mlstatic.com/D_NQ_NP_886172-MLA41120243160_032020-O.webp', '5', 'Tipo de resolución 4k,  Pantalla: OLED 65\", Resolucion máxima: 3840 x 2160 px, Dimensiones: 145 x 5 x 83 cm.', 6);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
