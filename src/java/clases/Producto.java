/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author Fer
 */
public class Producto {

    public String nombre;
    public String foto;
    public String descripcion;
    public String precio;
    public String cantidad;
    public int id;

    public String hacerHTML() {
        return "<div class=\"products\">\n"
                + "        <div class=\"product-box\"\n"
                + "             ng-repeat=\"product in products\">\n"
                + "          <div class='title'>" + this.nombre + "</div>\n"
                + "          <div class='show-base'>\n"
                + "            <img   src=\"" + this.foto + "\"" + this.nombre + " />\n"
                + "            <div class=\"mask\">\n"
                + "              <h2>" + this.nombre + "</h2>\n"
                + "              <p ng-class=\"{old:product.specialPrice}\">$" + this.precio + " </p>\n"
                + "              <p ng-show=\"product.specialPrice\"> " + this.cantidad + " </p>\n"
                + "              <div class=\"description\">\n"
                + "                " + this.descripcion + "\n"
                + "              </div>\n"
                + "              <div>\n"
                + "                <a href=\"#\" class=\"more\">Info</a>\n"
                + "                <a href=\"#\" class=\"tocart\">Comprar</a>\n"
                + "              </div>\n"
                + "            </div>\n"
                + "          </div>\n"
                + "        </div>\n"
                + "      </div>";
    }

    public void validarFoto() {
        if (this.foto == null || this.foto.isEmpty()) {
            this.foto = "https://us.123rf.com/450wm/naropano/naropano1606/naropano160600550/58727711-fondo-gris-oscuro-el-dise%C3%B1o-de-textura-fondo-del-grunge-.jpg?ver=6";
        }
    }

}
