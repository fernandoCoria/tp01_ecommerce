/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

/**
 *
 * @author Fer
 */
public class Cadenas {
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    public String cabeceraHTML() {
        return "<!DOCTYPE html>\n"
                + "<html>\n"
                + "<head>\n"
                + "    <meta charset='utf-8'>\n"
                + "    <meta http-equiv='X-UA-Compatible' content='IE=edge'>\n"
                + "    <title>Listado de productos</title>\n"
                + "    <meta name='viewport' content='width=device-width, initial-scale=1'>\n"
                + "    <link rel='stylesheet' type='text/css' media='screen' href='css/productos.css'>\n"
                + "    <script src='main.js'></script>\n"
                + "</head>\n"
                + "<body>\n";
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    public String mostrarEncabezado() {
        return "<header> </header>";
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    public String finalizarHTML() {
        return "<footer></footer>\n"
                + "<script>\n"
                + "   window.onload(function(){\n"
                + "   inicio.html();\n"
                + "});\n"
                + "</script>\n"
                + "</body>\n"
                + "</html>";
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

}
