/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import clases.Cadenas;
import clases.Producto;
import entidades.DB;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Fer
 */
public class ConsultaTVs extends HttpServlet {

    //Atributos ********************************************************************************
    private Connection miConexion = null;
    private Producto miProducto;
    private Cadenas chunk = new Cadenas();
    private ArrayList<Producto> lista = new ArrayList();

/////////////////////////////////////////////////////////////////////////////////////
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        try (PrintWriter out = response.getWriter()) {

            //********************************************************************************
            out.println(this.chunk.cabeceraHTML());
            out.println(this.chunk.mostrarEncabezado());
            out.println("<header></header>\n"
                    + "<main>"
                    + "<div class=\"wrap category-page\" ng-controller=\"Application.Controllers.Category.ProductList\">");
            //********************************************************************************            
            //Consulta -------------------------------------------------------
            try {
                this.miConexion = DB.getInstance().getConnection();
                PreparedStatement sqlListo = this.miConexion.prepareStatement("SELECT * FROM productos ORDER BY id DESC LIMIT 6");
                ResultSet resultados = sqlListo.executeQuery();

                while (resultados.next()) {
                    this.miProducto = new Producto();
                    this.miProducto.nombre = resultados.getString("nombre");
                    this.miProducto.precio = resultados.getString("precio");
                    this.miProducto.cantidad = resultados.getString("cantidad");
                    this.miProducto.descripcion = resultados.getString("descripcion");
                    this.miProducto.foto = resultados.getString("foto");
                    out.println(this.miProducto.hacerHTML());
                }
            } catch (Exception reporte) {
                System.out.println("Error en la consulta: " + reporte);
            } finally {

                try {
                    if (this.miConexion != null) {
                        this.miConexion.close();
                    }
                } catch (Exception reporte) {
                    System.out.println("Error en el cierre de la conexion: " + reporte);
                }
            }
            //Fin de consulta -------------------------------------------------------  

            //********************************************************************************
            out.println("</div>");
            out.println("</main>");
            this.chunk.finalizarHTML();


        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
