/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import clases.Producto;
import entidades.DB;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Fer
 */
public class ProductoNuevo extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            //////////////////////////////////////////////////////////////////////////////////////////
            Connection miConexion = null;
            Producto productoInsertar = new Producto();
//            productoInsertar.nombre = request.getParameter("nombre");
//            productoInsertar.precio = request.getParameter("precio");
//            productoInsertar.cantidad = request.getParameter("cantidad");
//            productoInsertar.descripcion = request.getParameter("descripcion");
            productoInsertar.foto = request.getParameter("foto");
            productoInsertar.validarFoto();

            String consultaSQL = consultaSQL = "INSERT INTO `productos` (`nombre`, `precio`, `foto`, `cantidad`, `descripcion`) VALUES "
                    + "(" + "'" + request.getParameter("nombre") + "', "
                    + "" + "'" + request.getParameter("precio") + "', "
                    + "" + "'" + productoInsertar.foto + "', "
                    + "" + "'" + request.getParameter("cantidad") + "', "
                    + "" + "'" + request.getParameter("descripcion") + "');";

            System.out.println(consultaSQL);
            //*****************************************************************************************

            try {
                miConexion = miConexion = DB.getInstance().getConnection();
                PreparedStatement consultaLista = miConexion.prepareStatement(consultaSQL);
                consultaLista.execute();
                out.println("Producto agregado con exito");
            } catch (Exception quePaso) {
                System.out.println("Error en la consulta: " + quePaso);
            } finally {
                try {
                    if (miConexion != null) {
                        miConexion.close();
                    }
                } catch (Exception reporte) {
                    System.out.println("Error al cerrar la conexion: " + reporte);
                }
            }

            //////////////////////////////////////////////////////////////////////////////////////////////  
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
